package com.company;

import java.util.LinkedList;
import java.util.Queue;

public class MyStack {
    Queue<Integer>q1=new LinkedList<Integer>();
    Queue<Integer>q2=new LinkedList<Integer>();
    int current_size;

    public MyStack() {
        current_size=0;
    }
    void push(int x){
        current_size++;
        q2.add(x);
        while (!q1.isEmpty()){
            q2.add(q1.peek());
            q1.remove();
        }
        Queue<Integer>q=q1;
        q1=q2;
        q2=q;
    }
    void pop(){
        if (q1.isEmpty())
            return;
        q1.remove();
        current_size--;
    }
    int top(){
        if (q1.isEmpty()) {
            return -1;
        }
        return q1.peek();
    }
    int size(){
        return current_size;
    }

}
