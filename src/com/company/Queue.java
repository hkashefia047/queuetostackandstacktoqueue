package com.company;

import java.util.Stack;

public class Queue {
    Stack<Integer> s1 = new Stack<>();
    Stack<Integer> s2 = new Stack<>();

    void enQueue(int x) {
        while (!s1.empty()) {
            s2.push(s1.pop());
        }
        s1.push(x);
        while (!s2.empty()) {
            s1.push(s2.pop());
        }
    }

    int deQueue() {
        if (s1.empty()) {
            System.out.println("Q is empty");
            System.exit(0);
        }
        int x = s1.peek();
        s1.pop();
        return x;

    }
}
